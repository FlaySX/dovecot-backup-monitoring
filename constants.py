MYSQL = {
    'get_domain'                 : "SELECT *      FROM Domain         WHERE domain    =    '{}'      limit 1",
    'get_domains_from_backendid' : "SELECT *      FROM Domain         WHERE backendID =     {}       ",
    'get_backend_from_backendid' : "SELECT *      FROM Backend        WHERE backendID =     {}       limit 1",
    'get_backend_from_name'      : "SELECT *      FROM Backend        WHERE name      LIKE '{}.ams%' limit 1",
    'get_users_from_domainid'    : "SELECT *      FROM User           WHERE domainID  =     {} ",
    'set_new_backend'            : "UPDATE Domain SET backendID={}    WHERE domain    =    '{}'      limit 1",
    'set_migration'              : "UPDATE Domain SET migrating='{}'  WHERE domain    =    '{}'      limit 1",
    'unset_migration'            : "UPDATE Domain SET migrating=NULL  WHERE domain    =    '{}'      limit 1",
    'suspend_user'               : "UPDATE User   SET status = 2      WHERE email     =    '{}'      limit 1",
    'unsuspend_user'             : "UPDATE User   SET status = 1      WHERE email     =    '{}'      limit 1",
}
