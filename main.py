import utils
import constants
import json
import os
import logging
import time
from datetime import datetime, date, timedelta

with open(os.path.realpath(__file__).rstrip(os.path.basename(__file__)) + "config.json") as config_file:
        Config = json.load(config_file)

#print(json.dumps(Config, indent=4))

utils.initiate_logger(Config.get('loglevel'), Config.get('verbose'), Config.get('logfile'))

mysql = utils.MySQL(
        ops={
            'user': Config.get('mysql_user'),
            'pass': Config.get('mysql_pass'),
            'host': Config.get('mysql_host'),
            'database': Config.get('mysql_db')
            }
)

def get_domains_from_sat(hostname=None, backendid=None):
    if not hostname and not backendid:
        logging.error("No hostname or backendid specified!")
        return False

    if backendid:
        logging.debug(f'Getting domains from backendID "{backendid}"')
        query = constants.MYSQL.get('get_domains_from_backendid').format(backendid)

    elif hostname:
        logging.debug(f'Getting domains from hostname "{hostname}"')
        hostname = hostname.split('.')[0]
        query = constants.MYSQL.get('get_backend_from_name').format(hostname)
        backendid = mysql.query(query)[0].get('backendID')
        query = constants.MYSQL.get('get_domains_from_backendid').format(backendid)
    
    return mysql.query(query)
        
def main():
    results = {
        'skipped': [],
        'found': [],
        'missing': [],
        'inconsistent': [],
        'late': [],
        'succeeded': []
    }

    date_format         = "%Y-%m-%d"
    today               = date.today()
    yesterday           = today - timedelta(days=1)

    hostname = Config.get('hostname')
    logging.info(f'Getting list of domains from "{hostname}"')
    domains = get_domains_from_sat(hostname=Config.get('hostname'))

    for domain in domains:
        #print (json.dumps(domain, indent=4))

        domain_name = domain.get('domain')
        id = domain.get('domainID')
        enabled = True if domain.get('enabled') == 1 else False

        if not enabled:
            logging.info(f'Skipping domain "{domain_name}" because it is disabled')
            results['skipped'].append(domain_name)
            continue

        logging.debug(f'Getting mailboxes from {domain_name}')
        query = constants.MYSQL.get('get_users_from_domainid').format(domain.get('domainID'))
        users = mysql.query(query)

        if not users:
            logging.info(f'Skipping domain "{domain_name}" because it has no users')
            results['skipped'].append(domain_name)
            continue

        for mailbox in users:
            mailbox_name = mailbox.get('email').split('@')[0]
            backup_location = '/'.join([Config.get('backup_location'), domain_name, mailbox_name])
            hash_start = '/'.join([backup_location, 'backup_start.hash'])
            hash_end   = '/'.join([backup_location, 'backup_end.hash'])

            try:
                backup = [d for d in os.listdir(backup_location) if d.startswith('backup')]
                if not backup or 'backup_start.hash' not in backup or 'backup_end.hash' not in backup:
                    logging.error(f'Missing "backup_start.hash" and/or "backup_end.hash" files in {backup_location}')
                    results['missing'].append(domain_name)
                    break

            except FileNotFoundError:
                logging.error(f"Could not access '{backup_location}'.")
                results['missing'].append(domain_name)
                break

            hashes = []

            for hash in [hash_start, hash_end]:
                try:
                    backup_date = time.strftime(date_format, time.gmtime(os.path.getmtime(hash)))
                    with open(hash_start) as reader:
                        for i in reader:
                            i = i.rstrip('\n')
                            hashes.append((i, backup_date))

                except FileNotFoundError:
                    logging.error('Could not find {hash}')
                    results['missing'].append(domain_name)
                    break

            if hashes[0][0] != hashes[1][0]:
                logging.warning(f'Back-up hashes are inconsistent for "{domain_name}"')
                results['inconsistent'].append(domain_name)
                break

            if hashes[0][1] != str(today) and hashes[0][1] != str(yesterday):
                logging.warning(f'Back-up timestamp ({hashes[0][1]}) is not from today ({today}) or yesterday ({yesterday})')
                results['late'].append(domain_name)
                break
                
            if hashes[1][1] != str(today) and hashes[1][1] != str(yesterday):
                logging.warning(f'Back-up timestamp ({hashes[1][1]}) is not from today ({today}) or yesterday ({yesterday})')
                results['late'].append(domain_name)
                break

        if domain_name not in results['skipped'] and domain_name not in results['missing']:
            results['found'].append(domain_name)

        if domain_name in results['found'] and domain_name not in results['inconsistent'] and domain_name not in results['late']:
            results['succeeded'].append(domain_name)
            
    
    zapi = utils.Zabbix(server=Config.get('zabbix_server'), hostname=Config.get('hostname'))
    zapi.send('dbm.missing', len(results['missing']))
    zapi.send('dbm.inconsistent', len(results['inconsistent']))
    zapi.send('dbm.late', len(results['late']))
    zapi.send('dbm.succeeded', len(results['succeeded']))
    zapi.send('dbm.skipped', len(results['skipped']))

    logging.info(f"Missing backups: {json.dumps(results['missing'])}")
    logging.info(f"Inconsistent backups: {json.dumps(results['inconsistent'])}")
    logging.info(f"Late backups: {json.dumps(results['late'])}")

if __name__ == '__main__':
    main()
