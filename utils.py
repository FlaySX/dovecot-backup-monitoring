import logging
import subprocess
import os
from mysql.connector      import MySQLConnection, Error

def initiate_logger(log_level, verbose, logfile):
    log_path = '/'.join(logfile.split('/')[:-1]) + '/'

    try:
        h = [logging.FileHandler(logfile)]
    except FileNotFoundError:
        logging.warning('Missing logfile! Will create a new one..')
        os.makedirs(log_path, 0o755)
        h = [logging.FileHandler(logfile)]

    if verbose:
        h.append(logging.StreamHandler())
    try:
        logging.basicConfig(
            level=log_level,
            format='%(asctime)s {:<15} \t {:<15} \t {} '.format('%(levelname)s', '%(module)s', '%(message)s'),
            datefmt='%a, %d %b %Y %H:%M:%S',
            handlers=h
        )
    except PermissionError:
        msg = "Error! PermissionError: Cannot create log_file: '{}'. Make sure you have the right permissions!"
        exit(msg.format(logfile))


class MySQL(object):
    def __init__(self, ops={'user': None, 'pass': None, 'host': None, 'database': None}):
        Data = {
            'username' : ops.get('user'),
            'password' : ops.get('pass'),
            'host'     : ops.get('host'),
            'database' : ops.get('database')
        }

        for key in Data:
            if not Data[key]:
                msg = (f"Could initiate MySQL, missing mysql {key}")
                logging.error(msg)
                exit(msg)


        self.MySQL = MySQLConnection(
            host     = Data['host'],
            database = Data['database'],
            user     = Data['username'],
            password = Data['password']
        )        

    def query(self, query, dictionary=True, commit=False):
        try:
            cursor = self.MySQL.cursor(dictionary=dictionary)
            cursor.execute(query)

            if commit:
                self.MySQL.commit()

            res = cursor.fetchall()
            return res

        except Error as e:
            if "No result set to fetch from" in str(e):
                return True
            else:
                logging.error(e)
                raise Exception(e)
    

class Zabbix(object):
    def __init__(self, server=None, timeout=5, hostname=None):
        logging.info('Checking if zabbix_sender is available')

        if not hostname or not server:
            logging.error('Missing zabbix confings!')

        self.hostname = hostname
        self.timeout = timeout
        self.server = server
        self.binary = True

        cmd = 'which zabbix_sender'
        try:
            a = subprocess.run(cmd.split(), timeout=self.timeout, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
            if a.returncode == 1:
                logging.error('Missing zabbix_sender command')
                self.binary = False
        except subprocess.TimeoutExpired:
            logging.warning(f"Timeout ({self.timeout}) executing '{cmd}'")

    def send(self, key, value):
        if not self.binary:
            logging.error('Could not send key/value to zabbix, missing binary!')
            return False

        cmd = f"zabbix_sender -z {self.server} -s {self.hostname} -k {key} -o "
        cmd = cmd.split()
        cmd.append(str(value))

        logging.info(f'Sending key ({key}) with value ({value}) to {self.server}')

        try:
            a = subprocess.run(cmd, timeout=self.timeout)
            if a.returncode == 1:
                logging.error(f'Stderr on command ({cmd})')
        except subprocess.TimeoutExpired:
            logging.warning(f"Timeout ({self.timeout}) while executing subprocess cmd ({cmd})")
